package com.bj.petmeetingapi.controller;

import com.bj.petmeetingapi.entity.Member;
import com.bj.petmeetingapi.model.Pet.PetCreateRequest;
import com.bj.petmeetingapi.service.MemberService;
import com.bj.petmeetingapi.service.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pet")
public class PetController {
    private final MemberService memberService;
    private final PetService petService;

    @PostMapping("/new/member-id/{memberId}")
    public String setPet(@PathVariable long memberId, @RequestBody PetCreateRequest request) {
        Member member = memberService.getData(memberId);
        petService.setPet(member, request);

        return "OK";
    }
}
