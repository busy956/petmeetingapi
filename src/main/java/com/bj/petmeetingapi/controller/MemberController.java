package com.bj.petmeetingapi.controller;

import com.bj.petmeetingapi.entity.Member;
import com.bj.petmeetingapi.model.Member.MemberChangeRequest;
import com.bj.petmeetingapi.model.Member.MemberCreateRequest;
import com.bj.petmeetingapi.model.Member.MemberItem;
import com.bj.petmeetingapi.model.Member.MemberResponse;
import com.bj.petmeetingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberCreateRequest request) {
        memberService.setMember(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        return memberService.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(long id) {
       return memberService.getMember(id);
    }

    @PutMapping("/age/{id}")
    public String putMember(long id, MemberChangeRequest request) {
        memberService.putMember(id, request);

        return "OK";
    }
}

