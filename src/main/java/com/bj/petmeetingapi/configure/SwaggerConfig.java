package com.bj.petmeetingapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "회원과 반려동물 관리 App",
                description = "회원이 키우는 반려동물 관리 프로그램",
                version = "v1"))
@RequiredArgsConstructor
@Configuration

public class SwaggerConfig {
    @Bean
    public GroupedOpenApi v1OpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("회원과 반려동물 관리 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
