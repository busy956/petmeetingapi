package com.bj.petmeetingapi.model.Pet;

import com.bj.petmeetingapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetCreateRequest {
    private Member member;
    private String petName;
    private String petType;
}
