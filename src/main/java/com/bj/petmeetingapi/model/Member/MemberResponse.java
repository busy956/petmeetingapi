package com.bj.petmeetingapi.model.Member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberResponse {
    private Long id;
    private String name;
    private Short age;
    private Boolean isMan;
    private String phoneNumber;
}
