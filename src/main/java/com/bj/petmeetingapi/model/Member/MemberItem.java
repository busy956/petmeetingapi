package com.bj.petmeetingapi.model.Member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberItem {
    private String name;
    private Short age;
    private Boolean isMan;
}
