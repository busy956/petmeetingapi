package com.bj.petmeetingapi.model.Member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberChangeRequest {
    private Short age;
}
