package com.bj.petmeetingapi.repository;

import com.bj.petmeetingapi.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet, Long>{
}
