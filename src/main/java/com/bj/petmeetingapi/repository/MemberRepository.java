package com.bj.petmeetingapi.repository;

import com.bj.petmeetingapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
