package com.bj.petmeetingapi.service;

import com.bj.petmeetingapi.entity.Member;
import com.bj.petmeetingapi.model.Member.MemberChangeRequest;
import com.bj.petmeetingapi.model.Member.MemberCreateRequest;
import com.bj.petmeetingapi.model.Member.MemberItem;
import com.bj.petmeetingapi.model.Member.MemberResponse;
import com.bj.petmeetingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id) {
       return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setAge(request.getAge());
        addData.setIsMan(request.getIsMan());
        addData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originData = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (Member member : originData) {
            MemberItem addItem = new MemberItem();
            addItem.setName(member.getName());
            addItem.setAge(member.getAge());
            addItem.setIsMan(member.getIsMan());

            result.add(addItem);
        }
        return result;
    }

    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setAge(originData.getAge());
        response.setIsMan(originData.getIsMan());
        response.setPhoneNumber(originData.getPhoneNumber());

        return response;
    }

    public void putMember(long id, MemberChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setAge(request.getAge());

        memberRepository.save(originData);
    }
}
