package com.bj.petmeetingapi.service;

import com.bj.petmeetingapi.entity.Member;
import com.bj.petmeetingapi.entity.Pet;
import com.bj.petmeetingapi.model.Pet.PetCreateRequest;
import com.bj.petmeetingapi.repository.MemberRepository;
import com.bj.petmeetingapi.repository.PetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PetService {
    private final PetRepository petRepository;

    public void setPet(Member member, PetCreateRequest request){
        Pet addData = new Pet();
        addData.setMember(member);
        addData.setPetName(request.getPetName());
        addData.setPetType(request.getPetType());

        petRepository.save(addData);
    }
}
